package org.snail.dao.sys;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import org.snail.domain.SysPermission;


@Mapper
public interface PermissionDao {

    List<SysPermission> getByMap(Map<String, Object> map);

    SysPermission getById(Long id);

    Integer create(SysPermission permission);

    int update(SysPermission permission);

    int delete(Integer id);

    List<SysPermission> getList();

    List<SysPermission> getByUserId(Long userId);

	List<SysPermission> getRootMenuByUserId(Long userId);

	List<SysPermission> getMenuChildren(Long id);

}