package org.snail.dao.sys;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.snail.domain.SysRole;


@Mapper
public interface SysRoleDao {

	List<SysRole> getByMap(Map<String, Object> map);
	SysRole getById(Long id);
	Long create(SysRole role);
	Long update(SysRole role);
	int delete(Long id);
	List<SysRole> getRolesByUserId(Long id);
	List<SysRole> getTableList(SysRole queryObj);
}