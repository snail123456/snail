package org.snail.dao.sys;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.snail.domail.json.TableUser;
import org.snail.domain.SysUser;

@Mapper//加上该注解才能使用@MapperScan扫描到
public interface SysUserDao {

	List<SysUser> getByMap(Map<String, Object> map);
	SysUser getById(Long id);
	Long create(SysUser user);
	Long update(SysUser user);
	Long delete(Long id);
	SysUser getByUserName(String userName);
	List<TableUser> getTableList(TableUser queryObj);
	Long bindRole(@Param("id")Long id,@Param("roleId")Long roleId);
	Long unbindRole(Long id);

}