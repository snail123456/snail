package org.snail.filter;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.CollectionUtils;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.snail.conf.ShiroUtil;
import org.snail.domain.SysUser;


public class RoleAuthorizationFilter  extends AuthorizationFilter {

	@Override
	protected boolean isAccessAllowed(ServletRequest request, ServletResponse response,
			Object o) throws Exception {
		Subject subject = getSubject(request, response);  
		String[] rolesArray = (String[]) o;  
	   
		SysUser user= (SysUser) ShiroUtil.getSession().getAttribute("curentUser");
	    if(user==null) {
	      return false;
	    }
		
	    if (rolesArray == null || rolesArray.length == 0) {  
			// no roles specified, so nothing to check - allow access.  
			return true;  
		}  

		Set<String> roles = CollectionUtils.asSet(rolesArray);  
		for (String role : roles) {  
			if (subject.hasRole(role)) {  
				return true;  
			}  
		}  
		return false;  
	}  

	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response)
			throws IOException {
		saveRequest(request);
		WebUtils.issueRedirect(request, response, "/gologin");
		return false;
	}
}
