package org.snail.conf;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snail.dao.sys.PermissionDao;
import org.snail.dao.sys.SysUserDao;
import org.snail.domain.SysPermission;
import org.snail.domain.SysRole;
import org.snail.domain.SysUser;
import org.springframework.beans.factory.annotation.Autowired;

public class ShiroRealm extends AuthorizingRealm {
    private Logger logger =  LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SysUserDao userService;
    @Autowired
    private PermissionDao permissionService;
    
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        logger.info("doGetAuthorizationInfo+"+principalCollection.toString());
        SysUser user = userService.getByUserName((String) principalCollection.getPrimaryPrincipal());

        //把principals放session中 key=userId value=principals
        ShiroUtil.getSession().setAttribute(String.valueOf(user.getId()),SecurityUtils.getSubject().getPrincipals());
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        //赋予角色
        for(SysRole userRole:user.getRoles()){
            info.addRole(userRole.getName());
        }
        //赋予权限
        for(SysPermission permission:permissionService.getByUserId(user.getId())){
//            if(StringUtils.isNotBlank(permission.getPermCode()))
                info.addStringPermission(permission.getPermission());
        }

        //设置登录次数、时间
//        userService.updateUserLogin(user);
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        logger.info("doGetAuthenticationInfo +"  + authenticationToken.toString());
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        String userName=token.getUsername();
        logger.info(userName+token.getPassword());
        SysUser currentUser = userService.getByUserName(userName.trim());
        if (currentUser != null) { 
            //设置用户session
        	List<SysPermission> permissions = permissionService.getByUserId(currentUser.getId());
            ShiroUtil.getSession().setAttribute("currentUser", currentUser);
            return new SimpleAuthenticationInfo(userName,currentUser.getPassword(),ByteSource.Util.bytes(currentUser.getSalt()),getName());
        } else {
            return null;
        }
    }

}