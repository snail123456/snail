package org.snail.conf;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.snail.domain.SysUser;

public class ShiroUtil {

	public static Subject getSubject(){
		return SecurityUtils.getSubject();
	}

	public static Session getSession(){
		return getSubject().getSession();
	}
	
	public static SysUser getCurrentUse(){
		return (SysUser) getSession().getAttribute("currentUser");
	}

}