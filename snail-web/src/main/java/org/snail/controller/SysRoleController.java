package org.snail.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snail.domain.PageBean;
import org.snail.domain.SysRole;
import org.snail.domain.SysUser;
import org.snail.service.SysRoleService;
import org.snail.util.PasswordHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;

@Controller
public class SysRoleController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SysRoleService roleService;
	
	@GetMapping(value = "sys/showRole")
	public String roleList(){
		return "admin/role_list";
	}
	
	@GetMapping(value = "sys/showRoleAdd")
	public String rolerAdd(Model model){
		return "admin/role_add";
	}
	
	@GetMapping(value = "sys/showRoleModify")
	public String roleModify(Model model,Long id){
		SysRole  role = roleService.getById(id);
		model.addAttribute("role",role);
		return "admin/role_add";
	}
	
	@GetMapping(value = "sys/showPermissions")
	public String setPermissions(Model model,Long id){
//		SysRole  role = roleService.getById(id);
//		model.addAttribute("role",role);
		return "admin/permission_set";
	}
	
	@PostMapping(value = "sys/addRole")
	@ResponseBody
	public Long addRole(SysRole role){
		return roleService.create(role);
	}
	
	@PostMapping(value = "sys/modifyRole")
	@ResponseBody
	public Long modifyRole(SysRole role){
		return roleService.modify(role);
	}
	
	@GetMapping(value = "sys/showRoleList")
	@ResponseBody
    public PageBean<SysRole> list(int currentPage,int pageSize,SysRole queryObj) {
		List<SysRole> lst = roleService.getTableListByPage(currentPage, pageSize,queryObj);
		PageInfo<SysRole> page = new PageInfo<SysRole>(lst);
		PageBean<SysRole> plist = new PageBean<SysRole>(page.getPrePage(), page.getPageSize(), lst, page.getTotal());
		plist.setCode("0");
		return plist;
    }
	
}