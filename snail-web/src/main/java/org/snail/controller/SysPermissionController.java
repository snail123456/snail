package org.snail.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SysPermissionController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@GetMapping(value = "sys/showPermission")
	public String permissionList(){
		return "admin/permission_list";
	}


}