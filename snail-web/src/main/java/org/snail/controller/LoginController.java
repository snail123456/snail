package org.snail.controller;




import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snail.conf.ShiroUtil;
import org.snail.domain.SysPermission;
import org.snail.domain.SysUser;
import org.snail.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



@Controller
public class LoginController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private PermissionService permissionService;
	
	@GetMapping(value = "/index")
	public String goIndex(@ModelAttribute("name")String name,Model model){
		model.addAttribute("mlist",getMenu());
		return "index";
	}

	@GetMapping(value = "/gologin")
	public String goLogin(){
		return "login";
	}

	@GetMapping(value = "/403")
	public String error403Page(){
		return "/error/403";
	}

	@PostMapping(value = "/login")
	public String toHome(SysUser user,Model model,RedirectAttributes  rModel,String rememberMe,HttpServletRequest request){
		/*String codeMsg = (String)request.getAttribute("shiroLoginFailure");
		if("code.error".equals(codeMsg)){
			model.addAttribute("message","验证码错误");
			return "/login";
		}*/
		UsernamePasswordToken token = new UsernamePasswordToken(user.getUsername().trim(),
				user.getPassword());
		Subject subject = SecurityUtils.getSubject();
		String msg=null;
		try{
			subject.login(token);
			//subject.hasRole("admin");
			if(subject.isAuthenticated()){
				rModel.addFlashAttribute("name", user.getUsername());
				return "redirect:index";
			}
		}catch (UnknownAccountException e) {
			msg = "用户名/密码错误";
		} catch (IncorrectCredentialsException e) {
			msg = "用户名/密码错误";
		} catch (ExcessiveAttemptsException e) {
			msg = "登录失败多次，账户锁定10分钟";
		}
		if(msg!=null){
			model.addAttribute("message",msg);
		}
		return "login";
	}
	
	public List<SysPermission> getMenu(){
		SysUser currentUser = ShiroUtil.getCurrentUse();
		List<SysPermission> mList= permissionService.getRootMenuByUserId(currentUser.getId());
		for(SysPermission sysMenu:mList){
			getChild(sysMenu);
		}
		return mList;
	}

	public SysPermission getChild(SysPermission sysMenu){
		List<SysPermission> mList=permissionService.getMenuChildren(sysMenu.getId());
		for(SysPermission menu:mList){
			SysPermission m=getChild(menu);
			sysMenu.addChild(m);
		}
		return sysMenu;
	}
	

}