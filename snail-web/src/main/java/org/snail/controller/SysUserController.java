package org.snail.controller;

import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snail.domail.json.TableUser;
import org.snail.domain.PageBean;
import org.snail.domain.SysRole;
import org.snail.domain.SysUser;
import org.snail.service.SysRoleService;
import org.snail.service.SysUserService;
import org.snail.util.PasswordHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.PageInfo;

@Controller
public class SysUserController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SysUserService userService;

	@Autowired
	private SysRoleService roleService;
	
	@GetMapping(value = "sys/showUser")
	public String userList(){
		return "admin/user_list";
	}
	
	@GetMapping(value = "sys/showUserAdd")
	public String userAdd(Model model){
		List<SysRole> roles = roleService.getByMap(null);
		model.addAttribute("roles",roles);
		return "admin/user_add";
	}
	
	@GetMapping(value = "sys/showUserModify")
	public String userModify(Model model,Long id){
		List<SysRole> allRoles = roleService.getByMap(null);
		List<SysRole> hasRoles = roleService.getRolesByUserId(id);
		SysUser user = userService.getById(id);
		for(SysRole role:allRoles){
			for(SysRole role2:hasRoles){
				if(role.getId()==role2.getId()){
					role.setChecked(true);
				}
			}
		}
		model.addAttribute("roles",allRoles);
		model.addAttribute("user",user);
		return "admin/user_add";
	}
	
	@PostMapping(value = "sys/addUser")
	@ResponseBody
	public Long addUser(SysUser user,@RequestParam(value = "roleIds[]",required=false)Long []roleIds){
		PasswordHelper.encryptPassword(user);
		return userService.create(user,roleIds);
	}
	
	@PostMapping(value = "sys/modifyUser")
	@ResponseBody
	public Long modifyUser(SysUser user,@RequestParam(value = "roleIds[]",required=false)Long []roleIds){
		return userService.modify(user,roleIds);
	}
	
	@PostMapping(value = "sys/delAllUser")
	@ResponseBody
	public Long delAllUser(long idArr[]){
		return userService.dellAllUser(idArr);
	}
	
	@PostMapping(value = "sys/delUser")
	@ResponseBody
	public Long delAllUser(Long id){
		return userService.delete(id);
	}
	
	@GetMapping(value = "sys/showUserList")
	@ResponseBody
    public PageBean<TableUser> list(int currentPage,int pageSize,TableUser queryObj) {
		System.out.println(queryObj.getCnname());
		List<TableUser> lst = userService.getTableListByPage(currentPage, pageSize,queryObj);
		PageInfo<TableUser> page = new PageInfo<TableUser>(lst);
		PageBean<TableUser> plist = new PageBean<TableUser>(page.getPrePage(), page.getPageSize(), lst, page.getTotal());
		plist.setCode("0");
		return plist;
    }
	

}