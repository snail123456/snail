/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2018-04-24 18:28:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `permission_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES ('1', '1', '1');
INSERT INTO `role_permission` VALUES ('2', '1', '2');
INSERT INTO `role_permission` VALUES ('3', '1', '3');
INSERT INTO `role_permission` VALUES ('4', '1', '4');
INSERT INTO `role_permission` VALUES ('5', '1', '5');
INSERT INTO `role_permission` VALUES ('6', '1', '6');
INSERT INTO `role_permission` VALUES ('7', '1', '7');
INSERT INTO `role_permission` VALUES ('8', '1', '8');
INSERT INTO `role_permission` VALUES ('9', '1', '9');

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `p_id` bigint(20) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `order_num` int(4) DEFAULT NULL COMMENT '排序字段',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `create_by` varchar(32) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL COMMENT '权限',
  `menu_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1菜单2按钮',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES ('1', '系统管理', null, null, '1', '&#xe726;', null, null, null, null, null, '1');
INSERT INTO `sys_permission` VALUES ('2', '菜单管理', '1', 'sys/showPermission', '3', null, null, null, null, null, null, '1');
INSERT INTO `sys_permission` VALUES ('3', '用户管理', '1', 'sys/showUser', '4', null, null, null, null, null, null, '1');
INSERT INTO `sys_permission` VALUES ('4', '角色管理', '1', 'sys/showRole', '5', null, null, null, null, null, null, '1');
INSERT INTO `sys_permission` VALUES ('5', '系统监控', null, null, '2', '&#xe6ce;', null, null, null, null, null, '1');
INSERT INTO `sys_permission` VALUES ('6', '系统日志', '5', '', '6', null, null, null, null, null, null, '1');
INSERT INTO `sys_permission` VALUES ('7', '接口api', '5', null, '7', null, null, null, null, null, null, '1');
INSERT INTO `sys_permission` VALUES ('8', '系统监控', '5', null, '8', null, null, null, null, null, null, '1');
INSERT INTO `sys_permission` VALUES ('9', '定时任务', '5', null, '9', '', null, null, null, null, null, '1');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `role_level` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `enable` int(1) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'admin', '超级管理员', null, 'a', '0', '2018-04-24 14:16:34');
INSERT INTO `sys_role` VALUES ('2', 'dev', '研发', null, 'b', '1', '2018-04-23 14:16:37');
INSERT INTO `sys_role` VALUES ('3', 'test', '测试', null, 'test', '1', '2018-04-24 18:18:14');
INSERT INTO `sys_role` VALUES ('4', 'product', '产品', null, '', '0', '2018-04-24 18:18:34');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cnname` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `mobile_phone` varchar(255) DEFAULT NULL,
  `wechat_id` int(11) DEFAULT NULL,
  `skill` varchar(255) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `login_count` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', null, 'admin', '2ca6415e85449cecc9569e6a50758afe', '053c6c332b2ee29f7a5edb2935a57f2c', null, '400-011-8882', '15211212121', null, null, null, null);
INSERT INTO `sys_user` VALUES ('14', '泰斯特001', 'test001', '5f862b4d70420044a2cebc601aa3a401', '613ca54dd6f878aa52da91d3b5ee36a5', '255@qq.com', null, '13521222222', null, null, null, null);
INSERT INTO `sys_user` VALUES ('15', '泰斯特002', 'test002', '054465cf34517e2e5985ac2582d63507', '416a6081522a2f8ae952216e23be3e6b', '99@qq.com', null, '15888888888', null, null, null, null);

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('1', '1', '1');
INSERT INTO `user_role` VALUES ('2', '1', '2');
INSERT INTO `user_role` VALUES ('15', '15', '1');
INSERT INTO `user_role` VALUES ('17', '14', '2');

-- ----------------------------
-- Procedure structure for getChildList
-- ----------------------------
DROP PROCEDURE IF EXISTS `getChildList`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getChildList`(IN rootId varchar(100) ,OUT str varchar(2000))
BEGIN   
DECLARE str varchar(2000);  
DECLARE cid varchar(2000);   
SET str = '$';   
SET cid = rootId;   
WHILE cid is not null DO     
    SET str = concat(str, ',',cid);
    SELECT group_concat(id) INTO cid FROM t_base_dept where FIND_IN_SET(pid, cid) > 0;      
END WHILE;   
SELECT  str;   
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for simpleproc
-- ----------------------------
DROP PROCEDURE IF EXISTS `simpleproc`;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `simpleproc`(OUT param1 INT)
BEGIN
select count(*) into param1 from user2;
end
;;
DELIMITER ;
