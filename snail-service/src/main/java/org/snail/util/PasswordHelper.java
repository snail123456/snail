package org.snail.util;

import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.snail.domain.SysUser;

public class PasswordHelper {
	 //随机数生成器
    private static RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();
	private static final String algorithmName = "md5";
	private  static final int hashIterations = 2;

	public static void encryptPassword(SysUser user) {
		// User对象包含最基本的字段Username和Password
		// 将用户的注册密码经过散列算法替换成一个不可逆的新密码保存进数据，散列过程使用了盐
		user.setSalt(randomNumberGenerator.nextBytes().toHex());
		String newPassword = new SimpleHash(algorithmName, user.getPassword(),user.getSalt(),hashIterations).toHex();
		user.setPassword(newPassword);
	}
	
}