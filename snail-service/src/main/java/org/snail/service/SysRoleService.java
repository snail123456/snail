package org.snail.service;

import java.util.List;
import java.util.Map;

import org.snail.dao.sys.SysRoleDao;
import org.snail.domail.json.TableUser;
import org.snail.domain.SysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;

@Service
public class SysRoleService {
    @Autowired
	private SysRoleDao roleDao;
	
	public List<SysRole> getByMap(Map<String,Object> map) {
	    return roleDao.getByMap(map);
	}
	
	public SysRole getById(Long id) {
		return roleDao.getById(id);
	}
	
	public Long create(SysRole role) {
		return roleDao.create(role);
	}
	
	public int delete(Long id) {
		return roleDao.delete(id);
	}

	public List<SysRole> getRolesByUserId(Long id) {
		return  roleDao.getRolesByUserId(id);
	}

	public List<SysRole> getTableListByPage(int currentPage, int pageSize, SysRole queryObj) {
		PageHelper.startPage(currentPage, pageSize, true);
		return roleDao.getTableList(queryObj);
	}

	public Long modify(SysRole role) {
		return roleDao.update(role);
	}
    
}