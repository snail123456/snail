package org.snail.service;

import java.util.List;
import java.util.Map;

import org.snail.dao.sys.SysUserDao;
import org.snail.domail.json.TableUser;
import org.snail.domain.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;


@Service
@Transactional
public class SysUserService {
	
    @Autowired
	private SysUserDao userDao;
    
	
	public List<SysUser> getByMap(Map map) {
		return userDao.getByMap(null);
	}
	
	public SysUser getById(Long id) {
		return userDao.getById(id);
	}
	
	public Long create(SysUser user,Long []roleIds) {
		Long result = userDao.create(user);
		if(roleIds!=null){
			for(Long roleId:roleIds){
				userDao.bindRole(user.getId(),roleId);
			}
		}
		return result;
	}
	
	public SysUser update(SysUser user) {
		userDao.update(user);
		return user;
	}
	
	public Long delete(Long id) {
		userDao.unbindRole(id);
		return userDao.delete(id);
	}

	public SysUser getByUserName(String userName) {
		return userDao.getByUserName(userName);
	}

	public List<TableUser> getTableListByPage(int currentPage, int pageSize,TableUser queryObj) {
		PageHelper.startPage(currentPage, pageSize, true);
		return userDao.getTableList(queryObj);
	}

	public Long dellAllUser(long[] idArr) {
		long count = 0;
		for(long id:idArr){
			count++;
			userDao.unbindRole(id);
			userDao.delete(id);
		}
		return count;
	}

	public Long modify(SysUser user, Long[] roleIds) {
		userDao.unbindRole(user.getId());
		if(roleIds!=null){
			for(Long roleId:roleIds){
				userDao.bindRole(user.getId(),roleId);
			}
		}
		return userDao.update(user);
	}

}
