package org.snail.service;

import java.util.List;
import java.util.Map;

import org.snail.dao.sys.PermissionDao;
import org.snail.domain.SysPermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PermissionService {
    @Autowired
	private PermissionDao permissionDao;
	
	public List<SysPermission> getByMap(Map<String,Object> map) {
	    return permissionDao.getByMap(map);
	}
	
	public SysPermission getById(Long id) {
		return permissionDao.getById(id);
	}
	
	public SysPermission create(SysPermission permission) {
		permissionDao.create(permission);
		return permission;
	}
	
	public SysPermission update(SysPermission permission) {
		permissionDao.update(permission);
		return permission;
	}
	
	public int delete(Integer id) {
		return permissionDao.delete(id);
	}

	public List<SysPermission> getRootMenuByUserId(Long userId) {
		return permissionDao.getRootMenuByUserId(userId);
	}

	public SysPermission selectByPrimaryKey(Long id) {
		return  permissionDao.getById(id);
	}

	public List<SysPermission> getMenuChildren(Long id) {
		return  permissionDao.getMenuChildren(id);
	}
    
}
